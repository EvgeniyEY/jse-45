package ru.ermolaev.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.ermolaev.tm.endpoint.soap.AuthenticationSoapEndpoint;
import ru.ermolaev.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.ermolaev.tm.endpoint.soap.TaskSoapEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
public class SoapConfig {

    @Bean
    public Endpoint projectEndpointRegistry(@NotNull final ProjectSoapEndpoint projectSoapEndpoint, @NotNull final Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, projectSoapEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(@NotNull final TaskSoapEndpoint taskSoapEndpoint, @NotNull final Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, taskSoapEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authenticationEndpointRegistry(@NotNull final AuthenticationSoapEndpoint auth, @NotNull final Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, auth);
        endpoint.publish("/AuthenticationEndpoint");
        return endpoint;
    }

}
