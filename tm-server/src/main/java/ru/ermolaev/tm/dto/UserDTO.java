package ru.ermolaev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.RoleType;

import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    private String login = "";

    private String passwordHash = "";

    private String email = "";

    private String firstName = "";

    private String middleName = "";

    private String lastName = "";

    private List<RoleType> roles = Collections.singletonList(RoleType.USER);

    private Boolean locked = false;

    @Override
    public String toString() {
        return "LOGIN: " + login + '\n' +
                "E-MAIL: " + email + '\n' +
                "FIRST-NAME: " + firstName + '\n' +
                "MIDDLE-NAME: " + middleName + '\n' +
                "LAST-NAME: " + lastName + '\n' +
                "ROLES: " + roles
                ;
    }

    @Nullable
    public static UserDTO toDTO(@Nullable final User user) {
        if (user == null) return null;
        return new UserDTO(user);
    }

    @NotNull
    public static List<UserDTO> toDTO(@Nullable final Collection<User> users) {
        if (users == null || users.isEmpty()) return Collections.emptyList();
        @NotNull final List<UserDTO> result = new ArrayList<>();
        for (@Nullable final User user : users) {
            if (user == null) continue;
            result.add(new UserDTO(user));
        }
        return result;
    }

    public UserDTO(@Nullable final User user) {
        if (user == null) return;
        id = user.getId();
        login = user.getLogin();
        passwordHash = user.getPasswordHash();
        email = user.getEmail();
        firstName = user.getFirstName();
        middleName = user.getMiddleName();
        lastName = user.getLastName();
//        roles = user.getRoles();
        locked = user.getLocked();
    }

}
