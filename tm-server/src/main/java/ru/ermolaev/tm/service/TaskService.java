package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.exception.incorrect.IncorrectCompleteDateException;
import ru.ermolaev.tm.exception.incorrect.IncorrectStartDateException;
import ru.ermolaev.tm.repository.ITaskRepository;

import java.util.Date;
import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    private final IProjectService projectService;

    private final IUserService userService;

    @Autowired
    public TaskService(
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectService projectService,
            @NotNull final IUserService userService
    ) {
        this.taskRepository = taskRepository;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Nullable
    @Override
    public Task getOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.getOne(id);
    }

    @Nullable
    @Override
    @Transactional
    public Task createTask(
            @Nullable final String userId,
            @Nullable final TaskDTO taskDTO
    ) throws Exception {
        if (userId == null) return null;
        if (taskDTO == null) return null;
        @Nullable final String name = taskDTO.getName();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final String projectId = taskDTO.getProjectId();
        if (projectId == null || projectId.isEmpty()) throw new EmptyNameException();
        @Nullable final String description = taskDTO.getDescription();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(userService.getOneById(userId));
        task.setProject(projectService.findOneById(userId, projectId));
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final TaskDTO taskDTO
    ) throws Exception {
        if (userId == null) return null;
        if (taskDTO == null) return null;
        @Nullable final String id = taskDTO.getId();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final String name = taskDTO.getName();
        if (name == null) throw new EmptyNameException();
        @Nullable final String description = taskDTO.getDescription();
        if (description == null) throw new EmptyDescriptionException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        if (!name.isEmpty()) task.setName(name);
        if (!description.isEmpty()) task.setDescription(description);
        if (name.isEmpty() && description.isEmpty()) return null;
        taskRepository.save(task);
        return task;
    }

    @Override
    @Transactional
    public void updateStartDate(
            @Nullable final String userId,
            @Nullable final TaskDTO taskDTO
    ) throws Exception {
        if (userId == null) return;
        if (taskDTO == null) return;
        @Nullable final String id = taskDTO.getId();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Date date = taskDTO.getStartDate();
        if (date == null) throw new EmptyDateException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return;
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        task.setStartDate(date);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void updateCompleteDate(
            @Nullable final String userId,
            @Nullable final TaskDTO taskDTO
    ) throws Exception {
        if (userId == null) return;
        if (taskDTO == null) return;
        @Nullable final String id = taskDTO.getId();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Date date = taskDTO.getCompleteDate();
        if (date == null) throw new EmptyDateException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return;
        if (task.getStartDate() == null) throw new IncorrectCompleteDateException(date);
        if (task.getStartDate().after(date)) throw new IncorrectCompleteDateException(date);
        task.setCompleteDate(date);
        taskRepository.save(task);
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Long countAllTasks() {
        return taskRepository.count();
    }

    @NotNull
    @Override
    public Long countByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.countByUserId(userId);
    }

    @NotNull
    @Override
    public Long countByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        return taskRepository.countByProjectId(projectId);
    }

    @NotNull
    @Override
    public Long countByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        return taskRepository.countByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public Task findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByUserIdAndName(userId, name);
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<TaskDTO> findAll() {
        return TaskDTO.toDTO(taskRepository.findAll());
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return TaskDTO.toDTO(taskRepository.findAllByUserId(userId));
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        return TaskDTO.toDTO(taskRepository.findAllByProjectId(projectId));
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        return TaskDTO.toDTO(taskRepository.findAllByUserIdAndProjectId(userId, projectId));
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeAllByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        taskRepository.deleteAllByProjectId(projectId);
    }

    @Override
    @Transactional
    public void removeAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        taskRepository.deleteAllByUserIdAndProjectId(userId, projectId);
    }

}
