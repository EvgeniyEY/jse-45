package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ermolaev.tm.TaskManagerApplication;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.RoleType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TaskManagerApplication.class)
public class UserServiceTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    private final static String admin = "admin";

    private final static String test = "test";

    private final static UserDTO userA = new UserDTO();

    private final static UserDTO userB = new UserDTO();

    private User createdUserA;

    private User createdUserB;

    @BeforeClass
    public static void prepare() {
        userA.setLogin(admin);
        userA.setPasswordHash(admin);
        userA.setEmail(admin);
        userA.setRoles(Collections.singletonList(RoleType.ADMINISTRATOR));
        userA.setFirstName(admin);
        userA.setMiddleName(admin);
        userA.setLastName(admin);

        userB.setLogin(test);
        userB.setPasswordHash(test);
        userB.setEmail(test);
        userB.setRoles(Collections.singletonList(RoleType.USER));
        userB.setFirstName(test);
        userB.setMiddleName(test);
        userB.setLastName(test);
    }

    @Before()
    public void prepareData() throws Exception {
        createdUserA = userService.create(userA);
        createdUserB = userService.create(userB);

        @NotNull final List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(admin, admin, authorities);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void clearData() {
        userService.removeAll();
    }

    @Test
    public void findOneByIdTest() throws Exception {
        @Nullable final User user = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), createdUserA.getId());
    }

    @Test
    public void findOneByLoginTest() throws Exception {
        @Nullable final User user = userService.findOneByLogin(createdUserA.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), createdUserA.getId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    public void removeOneByIdTest() throws Exception {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeOneById(createdUserA.getId());
        Assert.assertEquals(1, userService.findAll().size());
        @Nullable final User user = userService.findOneById(createdUserA.getId());
        Assert.assertNull(user);
    }

    @Test
    public void removeOneByLoginTest() throws Exception {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeOneByLogin(createdUserB.getLogin());
        Assert.assertEquals(1, userService.findAll().size());
        @Nullable final User user = userService.findOneById(createdUserB.getId());
        Assert.assertNull(user);
    }

    @Test
    public void removeAllTest() {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeAll();
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void createUserTest() throws Exception {
        @NotNull final UserDTO userDTO = new UserDTO();
        @NotNull final String login = "newUser";
        @NotNull final String pass = "pass";
        userDTO.setLogin(login);
        userDTO.setPasswordHash(pass);
        userService.create(userDTO);
        @Nullable final User user = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user);
    }

    @Test
    public void createUserWithLoginTest() throws Exception {
        @NotNull final String login = "newUser";
        @NotNull final String pass = "pass";
        userService.create(login, pass);
        @Nullable final User user = userService.findOneByLogin(login);
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
    }

    @Test
    public void createUserWithEmailTest() throws Exception {
        @NotNull final String login = "newUser";
        @NotNull final String pass = "pass";
        @NotNull final String email = "email";
        userService.create(login, pass, email);
        @Nullable final User user = userService.findOneByLogin(login);
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(email, user.getEmail());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, userService.count().intValue());
    }

    @Test
    public void updatePasswordTest() throws Exception {
        @Nullable final User user = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldPassword = user.getPasswordHash();
        userService.updatePassword(createdUserA.getId(), "newPassword");
        @Nullable final User user1 = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user1);
        @Nullable final String newPassword = user1.getPasswordHash();
        Assert.assertNotEquals(oldPassword, newPassword);
    }

    @Test
    public void updateUserFirstNameTest() throws Exception {
        @Nullable final User user = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldFirstName = user.getFirstName();
        userService.updateUserFirstName(createdUserA.getId(), "newFirstName");
        @Nullable final User user1 = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user1);
        @Nullable final String newFirstName = user1.getFirstName();
        Assert.assertNotEquals(oldFirstName, newFirstName);
        Assert.assertEquals(newFirstName, "newFirstName");
    }

    @Test
    public void updateUserMiddleNameTest() throws Exception {
        @Nullable final User user = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldMiddleName = user.getMiddleName();
        userService.updateUserMiddleName(createdUserA.getId(), "newMiddleName");
        @Nullable final User user1 = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user1);
        @Nullable final String newMiddleName = user1.getMiddleName();
        Assert.assertNotEquals(oldMiddleName, newMiddleName);
        Assert.assertEquals(newMiddleName, "newMiddleName");
    }

    @Test
    public void updateUserLastNameTest() throws Exception {
        @Nullable final User user = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldLastName = user.getLastName();
        userService.updateUserLastName(createdUserA.getId(), "newLastName");
        @Nullable final User user1 = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user1);
        @Nullable final String newLastName = user1.getLastName();
        Assert.assertNotEquals(oldLastName, newLastName);
        Assert.assertEquals(newLastName, "newLastName");
    }

    @Test
    public void updateUserEmailTest() throws Exception {
        @Nullable final User user = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldEmail = user.getEmail();
        userService.updateUserEmail(createdUserA.getId(), "newEmail");
        @Nullable final User user1 = userService.findOneById(createdUserA.getId());
        Assert.assertNotNull(user1);
        @Nullable final String newEmail = user1.getEmail();
        Assert.assertNotEquals(oldEmail, newEmail);
        Assert.assertEquals(newEmail, "newEmail");
    }

    @Test
    public void lockUserByLoginTest() throws Exception {
        Assert.assertFalse(userService.findOneById(createdUserA.getId()).getLocked());
        userService.lockUserByLogin(createdUserA.getLogin());
        Assert.assertTrue(userService.findOneById(createdUserA.getId()).getLocked());
        userService.unlockUserByLogin(createdUserA.getLogin());
        Assert.assertFalse(userService.findOneById(createdUserA.getId()).getLocked());
    }

}
