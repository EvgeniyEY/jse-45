package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.ermolaev.tm.TaskManagerApplication;
import ru.ermolaev.tm.entity.User;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TaskManagerApplication.class)
public class UserRepositoryTest {

    @Autowired
    private IUserRepository userRepository;

    private final User userA = new User();

    private final User userB = new User();

    @Before
    @Transactional
    public void prepareData() {
        userA.setLogin("admin");
        userRepository.save(userA);
        userB.setLogin("test");
        userRepository.save(userB);
    }

    @After
    @Transactional
    public void clear() {
        userRepository.deleteAll();
    }

    @Test
    public void findByLogin() {
        @Nullable final User user = userRepository.findByLogin(userA.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), userA.getId());
    }

    @Test
    @Transactional
    public void deleteByLogin() {
        Assert.assertEquals(2, userRepository.findAll().size());
        userRepository.deleteByLogin(userA.getLogin());
        Assert.assertEquals(1, userRepository.findAll().size());
        @Nullable final User user = userRepository.findById(userA.getId()).orElse(null);
        Assert.assertNull(user);
    }

}
