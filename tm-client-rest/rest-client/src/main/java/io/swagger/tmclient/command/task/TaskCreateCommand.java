package io.swagger.tmclient.command.task;

import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.TaskDTO;
import io.swagger.tmclient.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.tmclient.util.TerminalUtil;

@Component
public class TaskCreateCommand extends AbstractCommand {

    @Autowired
    public TaskCreateCommand(@NotNull final DefaultApi api) {
        super(api);
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create a new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        @Nullable final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER TASK NAME:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName(taskName);
        taskDTO.setProjectId(projectId);
        taskDTO.setDescription(description);
        api.createTask(taskDTO);
        System.out.println("[COMPLETE]");
    }

}
