package io.swagger.tmclient.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.tmclient.command.AbstractCommand;
import io.swagger.tmclient.exception.UnknownArgumentException;
import io.swagger.tmclient.exception.UnknownCommandException;
import io.swagger.tmclient.exception.EmptyCommandException;
import io.swagger.tmclient.util.TerminalUtil;

import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private List<AbstractCommand> commandList;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command: commandList) init(command);
    }

    private void init(@NotNull final AbstractCommand command) {
        commands.put(command.commandName(), command);
        arguments.put(command.arg(), command);
    }

    @SneakyThrows
    public void run(@Nullable final String[] args) {
        System.out.println("Welcome to task manager");
        initCommands(commandList);
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        if (arg == null) return false;
        try {
            parseArg(arg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    public void parseArg(@NotNull final String arg) throws Exception {
        if (arg.isEmpty()) return;
        @Nullable final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        argument.execute();
    }

    public void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) throw new EmptyCommandException();
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

}
